import { API_BASE_URL, ACCESS_TOKEN } from "../constants/Consts"

const request = (options) => {
    console.log(sessionStorage.getItem(ACCESS_TOKEN))
    const headers = new Headers({
        'Content-Type': 'application/json',
    })

    if (sessionStorage.getItem(ACCESS_TOKEN)) {
        headers.append('Authorization', 'Bearer ' + sessionStorage.getItem(ACCESS_TOKEN))
    }

    const defaults = { headers: headers };
    options = Object.assign({}, defaults, options);

    return fetch(options.url, options)
        .then(response =>
            response.json().then(json => {
                if (!response.ok) {
                    return Promise.reject(json);
                }
                return json;
            })
        );
};

export function login(loginRequest) {
    return request({
        url: API_BASE_URL + "/auth/signin",
        method: 'POST',
        body: JSON.stringify(loginRequest)
    });
}


export function getCurrentUser() {
    return request({
        url: API_BASE_URL + "/auth/user/me",
        method: 'GET'
    });
}