import React, { Component } from 'react';
import Login from '../auth/Login'
import { getCurrentUser } from '../util/Api'


export default class DashBoard extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isAuthenticated: false,
            currentUser: null
        };
    }

    componentDidMount() {
        this.loadCurrentUser();
    }

    loadCurrentUser = () => {
        getCurrentUser().then(response => {
            console.log("getuser", response);

            const user = Object.assign({}, response);
            this.setState({
                isAuthenticated: true,
                currentUser: user
            });
        })
    }

    handleLogin = (user) => {
        this.loadCurrentUser();
    }

    render() {
        console.log(this.state);
        let view = <Login onLogin={this.handleLogin} />;
        if (this.state.isAuthenticated && this.state.currentUser)
            view = <h1>خوش آمدی, {this.state.currentUser.username}</h1>
        return (
            <div>
                {view}
            </div>
        )
    }
}