const { override, fixBabelImports, addLessLoader } = require('customize-cra');

module.exports = override(
    fixBabelImports('import', {
        libraryName: 'antd',
        libraryDirectory: 'es',
        style: true,
    }),
    addLessLoader({
        javascriptEnabled: true,
        modifyVars: {
            // "@text-color": "#1890ff",
            // "@layout-body-background": "#FFFFFF",
            "@layout-header-background": "#A7E5F5",
            // "@layout-footer-background": "#FFFFFF"
        },
    }),
);