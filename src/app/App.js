import React, { Component } from 'react';
import { Layout, Menu, ConfigProvider } from 'antd'
import { BrowserRouter as Router, Route, Switch, useRouteMatch, useParams } from "react-router-dom"
import './App.css';
import AppHeader from '../common/AppHeader';
import DashBoard from '../dashboard/DashBoard';
import fr from 'antd/es/locale/fa_IR'
import 'moment/locale/fa'
import moment from 'moment';

moment.locale('fa');

const { Content } = Layout;


class App extends Component {

  render() {
    return (
      <Router>
        <ConfigProvider>
          <Layout className="app-container">
            <AppHeader />
            <Content className="app-content">
              <div className="container">
                <Switch>
                  <Route path="/dashboard" render={(props) => <DashBoard />} />
                </Switch>
              </div>
            </Content>
          </Layout>
        </ConfigProvider>
      </Router>
    );
  }

}

export default App;
