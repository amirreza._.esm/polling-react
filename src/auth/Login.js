import React, { Component } from 'react';
import { Form, Input, Icon, Button, notification } from 'antd';
import { login } from '../util/Api'
import './Login.css'
import { ACCESS_TOKEN } from '../constants/Consts';

export default class Login extends Component {

    render() {

        const AntdForm = Form.create()(LoginForm);

        return (
            <div className="login-form-container">
                <div className="login-banner">
                    ورود به پنل
                </div>
                <hr />
                <AntdForm onLogin={this.props.onLogin} />
            </div >
        )
    }
}

class LoginForm extends Component {

    componentDidMount() {

    }
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.form.validateFields((err, values) => {
            if (!err) {
                console.log('Received values of form: ', values);
                const loginRequest = Object.assign({}, values);
                login(loginRequest).then(response => {
                    console.log('res: ', response);
                    sessionStorage.setItem(ACCESS_TOKEN, response.token);
                    this.props.onLogin();
                }).catch(error => {
                    if (error.status === 401) {
                        notification.error({
                            message: 'Polling App',
                            description: 'Your Username or Password is incorrect. Please try again!'
                        });
                    } else {
                        notification.error({
                            message: 'Polling App',
                            description: error.message || 'Sorry! Something went wrong. Please try again!'
                        });
                    }
                });
            }
        });
    }

    render() {
        const { getFieldDecorator } = this.props.form;
        return (

            <Form
                onSubmit={this.handleSubmit}
                className="login-form"
                layout="horizontal">
                <Form.Item className="rtl" label="ایمیل">
                    {getFieldDecorator('email', {
                        rules: [{ required: true, message: "لطفا ایمیل خود را وارد کنید!" }]
                    })(<Input
                        name="email"
                        size="large"
                        className='input-ltr-ph-rtl'
                        suffix={<Icon type="user" />}
                        placeholder="ایمیل" />)
                    }
                </Form.Item>
                <Form.Item className="rtl" label="رمزعبور">
                    {getFieldDecorator('password', {
                        rules: [{ required: true, message: "لطفا رمزعبور خود را وارد کنید!" }]
                    })(
                        <Input
                            name="password"
                            size="large"
                            className="rtl"
                            suffix={<Icon type="lock" />}
                            placeholder="رمز عبور"
                            type="password" />
                    )}
                </Form.Item>
                <Form.Item>
                    <Button type="primary" htmlType="submit" className="login-form-button"
                        size="large">ورود</Button>
                </Form.Item>
            </Form>
        )
    }
}